# Jira - Squash SOAP to REST conversion

The main objective of this application is allow the use of [Jira 7](https://www.atlassian.com/software/jira) and more with [Squash TM](http://www.squashtest.org/en/decouvrir-squash-tm/contenu-statique/outils-et-fonctionnalites/squash-tm-test-management-en)
without having to buy the Squash TM license only for the REST API support.
 
## Informations

### Why not a full support of SOAP

Since the WSDL of Jira is in RPC/encoded which is a pain in the a** I choose to support only the minimal set of functions of the original WSDL.
The supported functions are :

 * login
 * addComment
 * createIssue
 * getComponents (always return an empty list)
 * getIssue
 * getIssueTypesForProject
 * getIssuesFromJqlSearch
 * getPriorities
 * getProjectByKey
 * getProjectsNoSchemes
 * getStatuses
 * getVersions

Exceptions are supported and transformed to fault.

Since the plugin JIRA for Squash use the url given to generate URL to the issue. A Servlet to redirect to the actual JIRA is needed.

### How to convert from SOAP to REST and back

SOAP Request are parsed to extract the arguments, the REST request is send to JIRA, the response is converted to a POJO which is injected into a StringTemplate of the SOAP response.

## Framework used

### StringTemplate

[StringTemplate](http://www.stringtemplate.org/) is used to inject the REST response in message to produce SOAP

### Jackson Databind

[Jackson Databind](https://github.com/FasterXML/jackson-databind) is used to convert JSON to POJO

### Retrofit

[Retrofit](https://square.github.io/retrofit/) is used to easyly create a client for the [REST API of JIRA](https://docs.atlassian.com/jira/REST/server/)

### Apache Commons

[Commons-lang](https://commons.apache.org/proper/commons-lang/) for String helpers and [commons-io](https://commons.apache.org/proper/commons-io/) for stream helpers

### Junit / AssertJ

[Junit](http://junit.org/junit4/) and [AssertJ](http://joel-costigliola.github.io/assertj/) as testing framework
