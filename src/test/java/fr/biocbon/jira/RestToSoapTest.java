package fr.biocbon.jira;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.apache.commons.io.IOUtils;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.biocbon.jira.rest.RestComment;
import fr.biocbon.jira.rest.RestEntity;
import fr.biocbon.jira.rest.RestIssue;
import fr.biocbon.jira.rest.RestMeta;
import fr.biocbon.jira.rest.RestPriority;
import fr.biocbon.jira.rest.RestProject;
import fr.biocbon.jira.rest.RestSearchResponse;
import fr.biocbon.jira.rest.RestStatus;
import fr.biocbon.jira.rest.RestVersion;
import fr.biocbon.jira.soap.SOAPServlet;

import java.io.InputStream;
import java.util.Collection;

@RunWith(Parameterized.class)
public class RestToSoapTest<T> {
    private String restMethodName;
    private String methodName;
    private String parameterName;
    private Class<T> restClass;

    public RestToSoapTest(String restMethodName, String methodName, String parameterName, Class<T> restClass) {
        this.restMethodName = restMethodName;
        this.methodName = methodName;
        this.parameterName = parameterName;
        this.restClass = restClass;
    }

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<Object[]> data() {
        return Lists.newArrayList(
                new Object[]{"comment", "addComment", null, RestComment.class},
                new Object[]{"issue", "createIssue", "issue", RestIssue.class},
                new Object[]{"issue", "getIssue", "issue", RestIssue.class},
                new Object[]{"components", "getComponents", null, RestEntity.class},
                new Object[]{"issueTypesForProject", "getIssueTypesForProject", "meta", RestMeta.class},
                new Object[]{"priority", "getPriorities", "priorities", RestPriority[].class},
                new Object[]{"project", "getProjectsNoSchemes", "projects", RestProject[].class},
                new Object[]{"projectByKey", "getProjectByKey", "project", RestProject.class},
                new Object[]{"search", "search", "result", RestSearchResponse.class},
                new Object[]{"status", "getStatuses", "statuses", RestStatus[].class},
                new Object[]{"version", "getVersions", "versions", RestVersion[].class}
        );
    }

    @Test
    public void test() throws Exception {
        fullFromRestToSoap(restMethodName, methodName, parameterName, restClass);
    }

    private T loadFromFile(Class<T> clazz, String name) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(name), clazz);
    }

    private void fullFromRestToSoap(String restMethodName, String methodName, String parameterName, Class<T> restClass) throws Exception {
        T o = loadFromFile(restClass, String.format("json/%s.json", restMethodName));
        String soapXML = SOAPServlet.generateSOAPResponse(methodName, parameterName, o);
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(String.format("soap/%s-response.xml", methodName));
        String expected =  IOUtils.toString(is, "UTF-8");
        Assertions.assertThat(soapXML).isXmlEqualTo(expected);
    }
}
