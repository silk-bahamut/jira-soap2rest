package fr.biocbon.jira.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import retrofit2.JiraRestFactory;

import org.junit.Test;

import fr.biocbon.jira.soap.TokenManager;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BindingTest {
    private static final DateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    @Test
    public void testComment() throws Exception {
        RestComment obj = loadFromFile(RestComment.class, "json/comment.json");
        assertEquals(obj.getBody(), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget venenatis elit. Duis eu justo eget augue iaculis fermentum. Sed semper quam laoreet nisi egestas at posuere augue semper.");
    }

    @Test
    public void testIssueCreate() throws Exception {
        RestIssue issue = loadFromFile(RestIssue.class, "json/issue.json");
        assertNotNull(issue);
        assertNotNull(issue.getFields());
        RestIssueFields fields = issue.getFields();
        assertEquals(fields.getDescription(), "# Cas de test: [INV10-F08-T101] Accéder avec un compte conforme\n# Exécution: http://192.168.1.30:8080/squash/executions/5131\n# Étape concernée: 2/5\n\n# Description de l'anomalie :\n\nBUG TEST - SQUASH JIRA");
        assertEquals(fields.getPriority().getId(), "1");
        assertEquals(fields.getProject().getKey(), "ERP");
        assertEquals(fields.getSummary(), "BUG TEST - SQUASH JIRA");
        assertEquals(fields.getIssuetype().getId(), "1");
        assertEquals(fields.getVersions().length, 1);
        assertEquals(fields.getVersions()[0].getId(), "11027");
    }

    @Test
    public void testIssueCreateResponse() throws Exception {
        RestIssueResponse issue = loadFromFile(RestIssueResponse.class, "json/issue-response.json");
        assertEquals(issue.getId(), "10000");
        assertEquals(issue.getKey(), "TST-24");

    }

    @Test
    public void testIssueType() throws Exception {
        RestMeta meta = loadFromFile(RestMeta.class, "json/issueTypesForProject.json");
        RestProject[] projects = meta.getProjects();
        assertEquals(projects.length, 1);
        RestProject project = projects[0];
        RestIssueType[] issueTypes = project.getIssuetypes();
        assertEquals(issueTypes.length, 11);

        RestIssueType issueType = issueTypes[0];
        assertEquals(issueType.isSubtask(), false);
        assertEquals(issueType.getDescription(), "Problème altérant ou compromettant les fonctions du produit.");
        assertEquals(issueType.getId(), "1");
        assertEquals(issueType.getName(), "Bogue");
        assertEquals(issueType.getIconUrl(), "http://jira:8080/images/icons/issuetypes/bug.png");
    }

    @Test
    public void testPriority() throws Exception {
        RestPriority[] lst = loadFromFile(RestPriority[].class, "json/priority.json");
        assertEquals(lst.length, 5);

        RestPriority prio = lst[0];
        assertEquals(prio.getDescription(), "Bloque le développement et/ou le travail de test, de telle sorte que la production n'a pas pu s'exécuter.");
        assertEquals(prio.getId(), "1");
        assertEquals(prio.getName(), "Blocage");
    }

    @Test
    public void testProject() throws Exception {
        RestProject[] lst = loadFromFile(RestProject[].class, "json/project.json");
        assertEquals(lst.length, 18);

        RestProject project = lst[0];
        assertEquals(project.getKey(), "CYL");
        assertEquals(project.getName(), "Cylande");
        assertEquals(project.getId(), "10203");
    }

    @Test
    public void testProjectByKey() throws Exception {
        RestProject project = loadFromFile(RestProject.class, "json/projectByKey.json");
        assertEquals(project.getKey(), "ERP");
        assertEquals(project.getName(), "ERP");
        assertEquals(project.getId(), "10015");
        assertEquals(project.getDescription(), "Ce projet est un développement interne ayant pour but de développer au sein de la DSI de Bio C'Bon, un ERP permettant de  :\r\n* gérer le référentiel article, fournisseur, sociétés (magasins, entrepôts)\r\n* gérer les conditions d'achats et les prix (prix d'achat, promotions, prix de vente)\r\n* gérer les stocks des différents magasins et des entrepôts\r\n* gérer les flux d'approvisionnement\r\n* gérer les approvisionnements\r\n* gérer les interfaces avec les applications externes");
        assertEquals(project.getLead().getName(), "m.brault");
        assertEquals(project.getUrl(), "http://web:8090/display/DSI/ERP");
        assertEquals(project.getSelf(), "http://jira:8080/rest/api/2/project/10015");
    }

    @Test
    public void testStatus() throws Exception {
        RestStatus[] lst = loadFromFile(RestStatus[].class, "json/status.json");
        assertEquals(lst.length, 56);

        RestStatus status = lst[0];
        assertEquals(status.getName(), "Ouverte");
        assertEquals(status.getIconUrl(), "http://jira:8080/images/icons/statuses/open.png");
        assertEquals(status.getDescription(), "La demande est ouverte et prête à être traitée par l'attribution.");
        assertEquals(status.getId(), "1");
    }

    @Test
    public void testVersion() throws Exception, ParseException {
        RestVersion[] lst = loadFromFile(RestVersion[].class, "json/version.json");
        assertEquals(lst.length, 18);

        RestVersion version = lst[0];
        assertEquals(version.getName(), "V1.0");
        assertEquals(version.getId(), "10008");
        assertEquals(version.isArchived(), false);
        assertEquals(version.isReleased(), false);
        assertEquals(SDF.format(version.getReleaseDate()), "2015-11-18");
    }

    @Test
    public void testSearchJQL() throws Exception {
        RestSearchResponse response = loadFromFile(RestSearchResponse.class, "json/search.json");
        assertNotNull(response);
        assertNotNull(response.getIssues());
        assertEquals(response.getIssues().length, 1);

        RestIssue issue = response.getIssues()[0];
        assertEquals(issue.getId(), "16307");
        assertEquals(issue.getKey(), "ERP-2537");
        assertEquals(issue.getSelf(), "http://jira:8080/rest/api/2/issue/16307");

        RestIssueFields fields = issue.getFields();
        assertNotNull(fields);
        assertEquals(fields.getDescription(), "# Cas de test: [INV10-F08-T101] Accéder avec un compte conforme\n# Exécution: http://192.168.1.30:8080/squash/executions/5131\n# Étape concernée: 2/5\n\n# Description de l'anomalie :\n\nBUG TEST - SQUASH JIRA");
        assertEquals(fields.getPriority().getId(), "1");
        assertEquals(fields.getProject().getKey(), "ERP");
        assertEquals(fields.getSummary(), "BUG TEST - SQUASH JIRA");
        assertEquals(fields.getIssuetype().getId(), "1");
        assertEquals(fields.getVersions().length, 1);
        assertEquals(fields.getVersions()[0].getId(), "11027");
        assertEquals(fields.getStatus().getId(), "1");
        assertEquals(fields.getReporter().getName(), "a.bousalem");
        assertEquals(SDF.format(fields.getCreated()), "2016-11-29");
        assertEquals(SDF.format(fields.getUpdated()), "2016-11-29");
    }


    @Test
//    @org.junit.Ignore("appel réel à jira, à n'utiliser que manuellement pour vérifier")
    public void testJira() throws IOException {
        TokenManager tm = new TokenManager();
        String t = tm.addAuthentication("jenkins", "Jenkins!");
        RestIssue resp = new JiraRestFactory("http://jira:8080/").getJiraRestClient().getIssue(tm.getAuthenticationByToken(t), "ERP-2577");
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(System.out, resp);
    }

    private <T> T loadFromFile(Class<T> clazz, String name) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(name), clazz);
    }
}
