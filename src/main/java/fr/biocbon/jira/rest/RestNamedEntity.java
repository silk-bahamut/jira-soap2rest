package fr.biocbon.jira.rest;

public class RestNamedEntity extends RestEntity {
    private String name;

    public RestNamedEntity() {
    }

    public RestNamedEntity(String id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
