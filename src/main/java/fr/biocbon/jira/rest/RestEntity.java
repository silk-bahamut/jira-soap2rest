package fr.biocbon.jira.rest;

public class RestEntity {
    private String id;

    public RestEntity() {
    }

    public RestEntity(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
