package fr.biocbon.jira.rest;

import java.util.Date;

public class RestIssueFields {

    private RestVersion[] versions;
    private String description;
    private RestEntity priority;
    private RestProject project;
    private String summary;
    private RestEntity issuetype;
    private RestEntity status;
    private RestNamedEntity reporter;
    private Date created;
    private Date updated;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public RestVersion[] getVersions() {
        return versions;
    }

    public void setVersions(RestVersion[] versions) {
        this.versions = versions;
    }

    public RestEntity getPriority() {
        return priority;
    }

    public void setPriority(RestEntity priority) {
        this.priority = priority;
    }

    public RestProject getProject() {
        return project;
    }

    public void setProject(RestProject project) {
        this.project = project;
    }

    public RestEntity getIssuetype() {
        return issuetype;
    }

    public void setIssuetype(RestEntity issuetype) {
        this.issuetype = issuetype;
    }

    public RestNamedEntity getReporter() {
        return reporter;
    }

    public void setReporter(RestNamedEntity reporter) {
        this.reporter = reporter;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public RestEntity getStatus() {
        return status;
    }
    public void setStatus(RestEntity status) {
        this.status = status;
    }
}
