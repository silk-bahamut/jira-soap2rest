package fr.biocbon.jira.rest;

public class RestProject extends RestNamedEntity {
    private String description;
    private String key;
    private RestUser lead;
    private String self;
    private String url;
    private RestIssueType[] issuetypes;

    public RestIssueType[] getIssuetypes() {
        return issuetypes;
    }

    public void setIssuetypes(RestIssueType[] issuetypes) {
        this.issuetypes = issuetypes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public RestUser getLead() {
        return lead;
    }

    public void setLead(RestUser lead) {
        this.lead = lead;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
