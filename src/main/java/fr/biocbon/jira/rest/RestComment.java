package fr.biocbon.jira.rest;

public class RestComment {
    private String body;

    public RestComment() {
    }

    public RestComment(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
