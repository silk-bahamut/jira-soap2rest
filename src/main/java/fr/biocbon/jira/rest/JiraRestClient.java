package fr.biocbon.jira.rest;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JiraRestClient {
    @GET("rest/api/2/myself")
    RestNamedEntity getMyself(@Header("Authorization") String auth);

    @GET("rest/api/2/project")
    RestProject[] getProjectsNoSchemes(@Header("Authorization") String auth);

    @GET("rest/api/2/status")
    RestStatus[] getStatuses(@Header("Authorization") String auth);

    @GET("rest/api/2/priority")
    RestPriority[] getPriorities(@Header("Authorization") String auth);

    @GET("rest/api/2/search")
    RestSearchResponse search(@Header("Authorization") String auth, @Query("jql") String jql);

    @GET("rest/api/2/project/{projectIdOrKey}/versions")
    RestVersion[] getVersions(@Header("Authorization") String auth, @Path("projectIdOrKey") String projectKey);

    @GET("rest/api/2/project/{projectIdOrKey}")
    RestProject getProjectByKey(@Header("Authorization") String auth, @Path("projectIdOrKey") String projectKey);

    @GET("rest/api/2/issue/createmeta")
    RestMeta getIssueTypesForProject(@Header("Authorization") String auth, @Query("projectKeys") String projectKey, @Query("projectIds") String projectIds);

    @POST("rest/api/2/issue")
    RestIssueResponse createIssue(@Header("Authorization") String auth, @Body RestIssue rIssue);

    @GET("rest/api/2/issue/{issueIdOrKey}")
    RestIssue getIssue(@Header("Authorization") String auth, @Path("issueIdOrKey") String issueIdOrKey);

    @POST("rest/api/2/issue/{issueIdOrKey}/comment")
    RestEntity addComment(@Header("Authorization") String auth, @Path("issueIdOrKey") String issueKey, @Body RestComment remoteComment);
}
