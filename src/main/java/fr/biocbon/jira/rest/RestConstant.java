package fr.biocbon.jira.rest;

public class RestConstant extends RestNamedEntity {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
