package fr.biocbon.jira.rest;

public class RestIssue extends RestEntity {
    private String key;
    private String self;
    private RestIssueFields fields;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public RestIssueFields getFields() {
        return fields;
    }

    public void setFields(RestIssueFields fields) {
        this.fields = fields;
    }
}
