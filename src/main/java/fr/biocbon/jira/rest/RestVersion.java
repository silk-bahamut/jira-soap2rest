package fr.biocbon.jira.rest;

import java.util.Date;

public class RestVersion extends RestNamedEntity {
    private boolean archived;
    private Date releaseDate;
    private boolean released;

    public RestVersion() {
    }

    public RestVersion(String id, String name) {
        super(id, name);
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public boolean isReleased() {
        return released;
    }

    public void setReleased(boolean released) {
        this.released = released;
    }
}
