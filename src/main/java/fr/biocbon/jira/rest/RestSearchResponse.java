package fr.biocbon.jira.rest;

public class RestSearchResponse {
    private RestIssue[] issues;

    public RestIssue[] getIssues() {
        return issues;
    }

    public void setIssues(RestIssue[] issues) {
        this.issues = issues;
    }
}
