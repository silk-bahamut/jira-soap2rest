package fr.biocbon.jira.rest;

public class RestMeta {
    private RestProject[] projects;

    public RestProject[] getProjects() {
        return projects;
    }

    public void setProjects(RestProject[] projects) {
        this.projects = projects;
    }
}
