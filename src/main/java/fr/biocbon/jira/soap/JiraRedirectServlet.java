package fr.biocbon.jira.soap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JiraRedirectServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String s = req.getRequestURL().toString();
        resp.sendRedirect("http://jira:8080/browse/" + s.substring(s.lastIndexOf('/') + 1));
    }
}
