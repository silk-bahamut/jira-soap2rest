package fr.biocbon.jira.soap;

import org.stringtemplate.v4.AttributeRenderer;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateRenderer implements AttributeRenderer {
    @Override
    public String toString(Object o, String s, Locale locale) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return f.format(o);
    }
}