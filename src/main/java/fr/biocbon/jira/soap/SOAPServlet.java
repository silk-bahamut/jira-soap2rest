package fr.biocbon.jira.soap;

import retrofit2.JiraRestFactory;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;
import org.stringtemplate.v4.StringRenderer;

import fr.biocbon.jira.rest.JiraRestClient;
import fr.biocbon.jira.rest.RestComment;
import fr.biocbon.jira.rest.RestEntity;
import fr.biocbon.jira.rest.RestIssue;
import fr.biocbon.jira.rest.RestIssueFields;
import fr.biocbon.jira.rest.RestIssueResponse;
import fr.biocbon.jira.rest.RestMeta;
import fr.biocbon.jira.rest.RestNamedEntity;
import fr.biocbon.jira.rest.RestPriority;
import fr.biocbon.jira.rest.RestProject;
import fr.biocbon.jira.rest.RestSearchResponse;
import fr.biocbon.jira.rest.RestStatus;
import fr.biocbon.jira.rest.RestVersion;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SOAPServlet extends HttpServlet {
    private static final Pattern NAMESPACE_PATTERN = Pattern.compile("xmlns:([^=]+)=\"http://soap.rpc.jira.atlassian.com\"");
    private static final String IN0 = "in0";
    private static final String IN1 = "in1";

    private TokenManager tokenManager = new TokenManager();
    private JiraRestClient restClient = new JiraRestFactory("http://jira:8080").getJiraRestClient();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        IOUtils.copy(SOAPServlet.class.getResourceAsStream("/jira-soap-4.4.5.wsdl"), resp.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String out;
        try {
            String soap = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            String namespace = extractWithPattern(NAMESPACE_PATTERN, soap);
            String methodName = extractWithPattern(Pattern.compile("<" + namespace + ":([^ ]+)"), soap);

            String auth, projectKey;
            switch (methodName) {
                case "login":
                    String login = extract(IN0, soap);
                    String password = extract(IN1, soap);
                    String token = tokenManager.addAuthentication(login, password);
                    RestNamedEntity myself = restClient.getMyself(tokenManager.getAuthenticationByToken(token));
                    System.out.println(String.format("Login : %s ok", myself.getName()));
                    out = generateSOAPResponse(methodName, "token", token);
                    break;
                case "addComment":
                    auth = extractAndConvertToken(soap);
                    projectKey = extract(IN1, soap);
                    String body = extract("body", soap);
                    restClient.addComment(auth, projectKey, new RestComment(body));
                    out = generateSOAPResponse(methodName, null, null);
                    break;
                case "createIssue":
                    auth = extractAndConvertToken(soap);
                    RestIssue issue = new RestIssue();
                    RestIssueFields fields = new RestIssueFields();
                    issue.setFields(fields);
                    fields.setDescription(extract("description", soap));
                    fields.setPriority(new RestEntity(extract("priority", soap)));
                    fields.setProject(new RestProject());
                    fields.getProject().setKey(extract("project", soap));
                    fields.setSummary(extract("summary", soap));
                    fields.setIssuetype(new RestEntity(extract("type", soap)));
                    fields.setVersions(new RestVersion[]{new RestVersion(extract("id", soap), null)});
                    RestIssueResponse response = restClient.createIssue(auth, issue);
                    issue.setKey(response.getKey());
                    issue.setId(response.getId());
                    out = generateSOAPResponse(methodName, "issue", issue);
                    break;
                case "getComponents":
                    out = generateSOAPResponse(methodName, null, null);
                    break;
                case "getIssue":
                    auth = extractAndConvertToken(soap);
                    String issueKey = extract(IN1, soap);
                    RestIssue rIssue = restClient.getIssue(auth, issueKey);
                    out = generateSOAPResponse(methodName, "issue", rIssue);
                    break;
                case "getIssueTypesForProject":
                    auth = extractAndConvertToken(soap);
                    projectKey = extract(IN1, soap);
                    String projectId = null;
                    if (StringUtils.isNumeric(projectKey)) {
                        projectId = projectKey;
                        projectKey = null;
                    }
                    RestMeta meta = restClient.getIssueTypesForProject(auth, projectKey, projectId);
                    out = generateSOAPResponse(methodName, "meta", meta);
                    break;
                case "getIssuesFromJqlSearch":
                    auth = extractAndConvertToken(soap);
                    String jql = extract(IN1, soap);
                    RestSearchResponse result = restClient.search(auth, jql);
                    out = generateSOAPResponse("search", "result", result);
                    break;
                case "getPriorities":
                    auth = extractAndConvertToken(soap);
                    RestPriority[] priorities = restClient.getPriorities(auth);
                    out = generateSOAPResponse(methodName, "priorities", priorities);
                    break;
                case "getProjectByKey":
                    auth = extractAndConvertToken(soap);
                    projectKey = extract(IN1, soap);
                    RestProject project = restClient.getProjectByKey(auth, projectKey);
                    out = generateSOAPResponse(methodName, "project", project);
                    break;
                case "getProjectsNoSchemes":
                    auth = extractAndConvertToken(soap);
                    RestProject[] projects = restClient.getProjectsNoSchemes(auth);
                    out = generateSOAPResponse(methodName, "projects", projects);
                    break;
                case "getStatuses":
                    auth = extractAndConvertToken(soap);
                    RestStatus[] statuses = restClient.getStatuses(auth);
                    out = generateSOAPResponse(methodName, "statuses", statuses);
                    break;
                case "getVersions":
                    auth = extractAndConvertToken(soap);
                    projectKey = extract(IN1, soap);
                    RestVersion[] versions = restClient.getVersions(auth, projectKey);
                    out = generateSOAPResponse(methodName, "versions", versions);
                    break;
                default:
                    throw new UnsupportedOperationException(String.format("Method '%s' is not supported", methodName));
            }
        } catch (Exception e) {
            e.printStackTrace();
            out = generateSOAPResponse("exception", "exception", e);
        }
        IOUtils.write(out, resp.getWriter());
    }

    private String extractAndConvertToken(String soap) {
        String token = extract(IN0, soap);
        return tokenManager.getAuthenticationByToken(token);
    }

    private String extract(String xml, String soap) {
        Pattern p = Pattern.compile(String.format("<%1$s.+>(.+)</%1$s>", xml), Pattern.DOTALL);
        return extractWithPattern(p, soap);
    }

    private String extractWithPattern(Pattern p, String soap) {
        String methodName = null;
        Matcher m = p.matcher(soap);
        if (m.find()) {
            methodName = m.group(1);
        }
        return methodName;
    }

    public static String generateSOAPResponse(String method, String parameterName, Object parameter) {
        URL url = SOAPServlet.class.getResource(String.format("/soap/response/%s.st", method));
        STGroup group = new STGroupFile(url, "UTF-8", '$', '$');
        group.registerRenderer(Date.class, new DateRenderer());
        group.registerRenderer(String.class, new StringRenderer());
        ST xmlMessage = group.getInstanceOf("build");
        if (parameterName != null) {
            xmlMessage.add(parameterName, parameter);
        }
        return xmlMessage.render();
    }
}
