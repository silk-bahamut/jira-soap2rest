package fr.biocbon.jira.soap;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class TokenManager {
    private long seq = 0L;
    private Map<String, String> cache = new HashMap<>();

    public String getAuthenticationByToken(String token) {
        return cache.get(token);
    }

    public String addAuthentication(String username, String password) {
        String auth64 = "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
        String token;
        synchronized (this) {
            token = Long.toString(seq++);
            cache.put(token, auth64);
        }
        return token;
    }

    public void removeToken(String token) {
        synchronized (this) {
            cache.remove(token);
        }
    }

    public void purgeOlder() {
        synchronized (this) {
            cache.entrySet().removeIf(entry -> Long.parseLong(entry.getKey()) < seq - 1000);
        }
    }
}
