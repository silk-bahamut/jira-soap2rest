package retrofit2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.converter.jackson.JacksonConverterFactory;

import fr.biocbon.jira.rest.JiraRestClient;

import java.util.concurrent.TimeUnit;

public class JiraRestFactory {
    private static final int DEFAULT_TIMEOUT_READ = 10;
    private static final int DEFAULT_TIMEOUT_CONNECT = 30;

    private Retrofit retrofit;
    private JiraRestClient service;

    private final String baseUrl;
    private final int readTimeout;
    private final int connectTimeout;

    public JiraRestFactory(String baseUrl) {
        this.baseUrl = baseUrl;
        this.readTimeout = DEFAULT_TIMEOUT_READ;
        this.connectTimeout = DEFAULT_TIMEOUT_CONNECT;
    }

    public JiraRestClient getJiraRestClient() {
        if (service == null) {
            service = getRetrofit().create(JiraRestClient.class);
        }
        return service;
    }

    private Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(buildConverterFactory())
                    .addCallAdapterFactory(new DirectCallAdapterFactory(response -> {
                        throw new IllegalStateException(response.errorBody().toString());
                    }))
                    .client(buildHttpClient())
                    .build();
        }
        return retrofit;
    }

    private OkHttpClient buildHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                .followRedirects(false)
                .addInterceptor(chain -> {
                    Request original = chain.request();

                    Request request = original.newBuilder()
                            .header("Accept", "application/json")
                            .header("Content-Type", "application/json")
                            .method(original.method(), original.body())
                            .build();
                    return chain.proceed(request);
                })
                .build();
    }

    private Converter.Factory buildConverterFactory() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return JacksonConverterFactory.create(objectMapper);
    }
}