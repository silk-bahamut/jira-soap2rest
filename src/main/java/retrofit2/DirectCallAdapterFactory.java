package retrofit2;


import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class DirectCallAdapterFactory extends CallAdapter.Factory {
    final DirectCallAdapter.ErrorHandler errorHandler;

    public DirectCallAdapterFactory(DirectCallAdapter.ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    @Override
    public CallAdapter<?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        if (Utils.getRawType(returnType) != Call.class) {
            return new DirectCallAdapter(retrofit, returnType, errorHandler);
        } else {
            return DefaultCallAdapterFactory.INSTANCE.get(returnType, annotations, retrofit);
        }
    }
}