package retrofit2;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;

public class DirectCallAdapter implements CallAdapter<Object> {

    private final Type type;
    private final ErrorHandler errorHandler;

    public DirectCallAdapter(Retrofit retrofit, Type type, ErrorHandler errorHandler) {
        this.type = type;
        this.errorHandler = errorHandler;
    }

    @Override
    public Type responseType() {
        return type;
    }

    @Override
    public <R> R adapt(Call<R> call) {
        Response<R> response;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {

                switch (response.code()) {
                    case HttpURLConnection.HTTP_FORBIDDEN:
                        throw new RestClientException("FORBIDDEN", "Not Allowed");
                    case HttpURLConnection.HTTP_UNAUTHORIZED:
                        throw new RestClientException("UNAUTHORIZED", "Not Logged");
                    case HttpURLConnection.HTTP_NOT_FOUND:
                        throw new RestClientException("NOT_FOUND", "Service unavailable");
                    default:
                        errorHandler.handleError(response);
                        return (R) null;
                }

            }
        } catch (IOException e) {
            throw new RestClientException(e);
        }
    }

    public interface ErrorHandler {
        void handleError(Response<?> response);
    }
}
