package retrofit2;

public class RestClientException extends RuntimeException {

    private String code;

    public RestClientException(String code, String message) {
        super(message);
        this.code = code;
    }

    public RestClientException(Throwable cause) {
        super(cause);
    }

    public String getCode() {
        return code;
    }
}
