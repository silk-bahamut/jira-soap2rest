build(projects) ::= <<
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
        <ns1:getProjectsNoSchemesResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://soap.rpc.jira.atlassian.com">
            <getProjectsNoSchemesReturn soapenc:arrayType="ns2:RemoteProject[$length(projects)$]" xsi:type="soapenc:Array" xmlns:ns2="http://beans.soap.rpc.jira.atlassian.com"
                                        xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
$projects: {project|
                <getProjectsNoSchemesReturn href="#id$i0$"/>
}; separator="\n"$
            </getProjectsNoSchemesReturn>
        </ns1:getProjectsNoSchemesResponse>
$projects: {project|
        <multiRef id="id$i0$" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns1$i0$:RemoteProject" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
                  xmlns:ns1$i0$="http://beans.soap.rpc.jira.atlassian.com">
            <description xsi:type="xsd:string">$project.description; format="xml-encode"$</description>
            <id xsi:type="xsd:string">$project.id$</id>
            <issueSecurityScheme xsi:type="ns1$i0$:RemoteScheme" xsi:nil="true"/>
            <key xsi:type="xsd:string">$project.key$</key>
            <lead xsi:type="xsd:string">$project.lead.name$</lead>
            <name xsi:type="xsd:string">$project.name; format="xml-encode"$</name>
            <notificationScheme xsi:type="ns1$i0$:RemoteScheme" xsi:nil="true"/>
            <permissionScheme xsi:type="ns1$i0$:RemotePermissionScheme" xsi:nil="true"/>
            <projectUrl xsi:type="xsd:string">$project.url$</projectUrl>
            <url xsi:type="xsd:string">http://jira:8080/browse/$project.key$</url>
        </multiRef>
}; separator="\n"$
    </soapenv:Body>
</soapenv:Envelope>
>>