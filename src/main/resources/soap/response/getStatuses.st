build(statuses) ::= <<
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
        <ns1:getStatusesResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://soap.rpc.jira.atlassian.com">
            <getStatusesReturn soapenc:arrayType="ns2:RemoteStatus[$length(statuses)$]" xsi:type="soapenc:Array" xmlns:ns2="http://beans.soap.rpc.jira.atlassian.com"
                               xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
$statuses: {status|
                <getStatusesReturn href="#id$i0$"/>
}; separator="\n"$
            </getStatusesReturn>
        </ns1:getStatusesResponse>
$statuses: {status|
        <multiRef id="id36" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns1$i0$:RemoteStatus" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
                  xmlns:ns1$i0$="http://beans.soap.rpc.jira.atlassian.com">
$if(status.description)$
            <description xsi:type="xsd:string">$status.description; format="xml-encode"$</description>
$else$
            <description xsi:type="xsd:string" xsi:nil="true"/>
$endif$
            <icon xsi:type="xsd:string">$status.iconUrl$</icon>
            <id xsi:type="xsd:string">$status.id$</id>
            <name xsi:type="xsd:string">$status.name; format="xml-encode"$</name>
        </multiRef>
}; separator="\n"$
    </soapenv:Body>
</soapenv:Envelope>
>>