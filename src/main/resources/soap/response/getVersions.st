build(versions) ::= <<
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
        <ns1:getVersionsResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://soap.rpc.jira.atlassian.com">
            <getVersionsReturn soapenc:arrayType="ns2:RemoteVersion[$length(versions)$]" xsi:type="soapenc:Array" xmlns:ns2="http://beans.soap.rpc.jira.atlassian.com"
                               xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
$versions: {version|
                <getVersionsReturn href="#id$i0$"/>
}; separator="\n"$
            </getVersionsReturn>
        </ns1:getVersionsResponse>
$versions: {version|
        <multiRef id="id$i0$" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns1$i0$:RemoteVersion" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
                  xmlns:ns1$i0$="http://beans.soap.rpc.jira.atlassian.com">
            <archived xsi:type="xsd:boolean">$version.archived$</archived>
            <id xsi:type="xsd:string">$version.id$</id>
            <name xsi:type="xsd:string">$version.name; format="xml-encode"$</name>
$if(version.releaseDate)$
            <releaseDate xsi:type="xsd:dateTime">$version.releaseDate$</releaseDate>
$else$
            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
$endif$
            <released xsi:type="xsd:boolean">$version.released$</released>
            <sequence xsi:type="xsd:long">$i$</sequence>
        </multiRef>
}; separator="\n"$
    </soapenv:Body>
</soapenv:Envelope>
>>