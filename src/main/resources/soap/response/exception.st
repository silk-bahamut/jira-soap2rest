build(exception) ::= <<
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soapenv:Body>
      <soapenv:Fault>
         <faultcode>soapenv:Server.userException</faultcode>
         <faultstring>$exception.message; format="xml-encode"$</faultstring>
         <detail>
            <com.atlassian.jira.rpc.exception.RemoteAuthenticationException xsi:type="ns1:$exception.class.simpleName$" xmlns:ns1="http://exception.rpc.jira.atlassian.com"/>
            <ns2:stackTrace xmlns:ns2="http://xml.apache.org/axis/">$exception.stackTrace: {stackTrace|$stackTrace$}; separator="\n"$</ns2:stackTrace>
            <ns3:hostname xmlns:ns3="http://xml.apache.org/axis/">BCB-SRV-JIRA</ns3:hostname>
         </detail>
      </soapenv:Fault>
   </soapenv:Body>
</soapenv:Envelope>
>>