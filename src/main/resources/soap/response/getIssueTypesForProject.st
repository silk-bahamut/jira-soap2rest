build(meta) ::= <<
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
        <ns1:getIssueTypesForProjectResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://soap.rpc.jira.atlassian.com">
            <getIssueTypesForProjectReturn soapenc:arrayType="ns2:RemoteIssueType[$length(first(meta.projects).issuetypes)$]" xsi:type="soapenc:Array" xmlns:ns2="http://beans.soap.rpc.jira.atlassian.com"
                                           xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
$first(meta.projects).issuetypes: {issuetype|
                <getIssueTypesForProjectReturn href="#id$i0$"/>
}; separator="\n"$
            </getIssueTypesForProjectReturn>
        </ns1:getIssueTypesForProjectResponse>
$first(meta.projects).issuetypes: {issuetype|
        <multiRef id="id$i0$" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns1$i0$:RemoteIssueType" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
                  xmlns:ns1$i0$="http://beans.soap.rpc.jira.atlassian.com">
            <description xsi:type="xsd:string">$issuetype.description; format="xml-encode"$</description>
            <icon xsi:type="xsd:string">$issuetype.iconUrl; format="xml-encode"$</icon>
            <id xsi:type="xsd:string">$issuetype.id$</id>
            <name xsi:type="xsd:string">$issuetype.name; format="xml-encode"$</name>
            <subTask xsi:type="xsd:boolean">$issuetype.subtask$</subTask>
        </multiRef>
}; separator="\n"$
    </soapenv:Body>
</soapenv:Envelope>
>>