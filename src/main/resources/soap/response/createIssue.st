build(issue) ::= <<
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
        <ns1:createIssueResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://soap.rpc.jira.atlassian.com">
            <createIssueReturn href="#id0"/>
        </ns1:createIssueResponse>
        <multiRef id="id0" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns2:RemoteIssue" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
                  xmlns:ns2="http://beans.soap.rpc.jira.atlassian.com">
            <affectsVersions soapenc:arrayType="ns2:RemoteVersion[1]" xsi:type="soapenc:Array">
                <affectsVersions href="#id1"/>
            </affectsVersions>
            <assignee xsi:type="xsd:string" xsi:nil="true"/>
            <attachmentNames soapenc:arrayType="xsd:string[0]" xsi:type="soapenc:Array"/>
            <components soapenc:arrayType="ns2:RemoteComponent[0]" xsi:type="soapenc:Array"/>
$if(issue.fields.created)$
            <created xsi:type="xsd:dateTime">$issue.fields.created$</created>
$else$
            <created xsi:type="xsd:dateTime" xsi:nil="true"/>
$endif$
            <description xsi:type="xsd:string">$issue.fields.description; format="xml-encode"$</description>
            <duedate xsi:type="xsd:dateTime" xsi:nil="true"/>
            <environment xsi:type="xsd:string" xsi:nil="true"/>
            <fixVersions soapenc:arrayType="ns2:RemoteVersion[0]" xsi:type="soapenc:Array"/>
            <id xsi:type="xsd:string">$issue.id$</id>
            <key xsi:type="xsd:string">$issue.key$</key>
            <priority xsi:type="xsd:string">$issue.fields.priority.id$</priority>
            <project xsi:type="xsd:string">$issue.fields.project.key$</project>
            <reporter xsi:type="xsd:string">$issue.fields.reporter.name$</reporter>
            <resolution xsi:type="xsd:string" xsi:nil="true"/>
            <status xsi:type="xsd:string">$issue.fields.status.id$</status>
            <summary xsi:type="xsd:string">$issue.fields.summary; format="xml-encode"$</summary>
            <type xsi:type="xsd:string">$issue.fields.issuetype.id$</type>
$if(issue.fields.updated)$
            <updated xsi:type="xsd:dateTime">$issue.fields.updated$</updated>
$else$
            <updated xsi:type="xsd:dateTime" xsi:nil="true"/>
$endif$
            <votes xsi:type="xsd:long">0</votes>
        </multiRef>
        <multiRef id="id1" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns5:RemoteVersion" xmlns:ns5="http://beans.soap.rpc.jira.atlassian.com"
                  xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
            <id xsi:type="xsd:string">$first(issue.fields.versions).id$</id>
        </multiRef>
    </soapenv:Body>
</soapenv:Envelope>
>>