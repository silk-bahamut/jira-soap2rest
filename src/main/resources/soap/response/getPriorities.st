build(priorities) ::= <<
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
        <ns1:getPrioritiesResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://soap.rpc.jira.atlassian.com">
            <getPrioritiesReturn soapenc:arrayType="ns2:RemotePriority[$length(priorities)$]" xsi:type="soapenc:Array" xmlns:ns2="http://beans.soap.rpc.jira.atlassian.com"
                                 xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
$priorities: {priority|
                <getPrioritiesReturn href="#id$i0$"/>
}; separator="\n"$
            </getPrioritiesReturn>
        </ns1:getPrioritiesResponse>
$priorities: {priority|
        <multiRef id="id$i0$" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns1$i0$:RemotePriority" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
                  xmlns:ns1$i0$="http://beans.soap.rpc.jira.atlassian.com">
            <color xsi:type="xsd:string">$priority.statusColor$</color>
            <description xsi:type="xsd:string">$priority.description; format="xml-encode"$</description>
            <icon xsi:type="xsd:string">$priority.iconUrl; format="xml-encode"$</icon>
            <id xsi:type="xsd:string">$priority.id$</id>
            <name xsi:type="xsd:string">$priority.name; format="xml-encode"$</name>
        </multiRef>
}; separator="\n"$
    </soapenv:Body>
</soapenv:Envelope>
>>