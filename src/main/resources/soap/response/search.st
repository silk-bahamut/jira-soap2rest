build(result) ::= <<
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
        <ns1:getIssuesFromJqlSearchResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://soap.rpc.jira.atlassian.com">
            <getIssuesFromJqlSearchReturn soapenc:arrayType="ns2:RemoteIssue[$length(result.issues)$]" xsi:type="soapenc:Array" xmlns:ns2="http://beans.soap.rpc.jira.atlassian.com"
                                          xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
$result.issues: {issue|
                <getIssuesFromJqlSearchReturn href="#id$i0$"/>
}; separator="\n"$
            </getIssuesFromJqlSearchReturn>
        </ns1:getIssuesFromJqlSearchResponse>
$result.issues: {issue|
        <multiRef id="id$i0$" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns1$i0$:RemoteIssue" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
                  xmlns:ns1$i0$="http://beans.soap.rpc.jira.atlassian.com">
            <affectsVersions soapenc:arrayType="ns1$i0$:RemoteVersion[1]" xsi:type="soapenc:Array">
                <affectsVersions href="#version$i0$"/>
            </affectsVersions>
            <assignee xsi:type="xsd:string" xsi:nil="true"/>
            <attachmentNames soapenc:arrayType="xsd:string[0]" xsi:type="soapenc:Array"/>
            <components soapenc:arrayType="ns1$i0$:RemoteComponent[0]" xsi:type="soapenc:Array"/>
            <created xsi:type="xsd:dateTime">2016-11-29T10:29:19.000Z</created>
            <customFieldValues soapenc:arrayType="ns1$i0$:RemoteCustomFieldValue[2]" xsi:type="soapenc:Array"/>
            <description xsi:type="xsd:string">$issue.fields.description; format="xml-encode"$</description>
            <duedate xsi:type="xsd:dateTime" xsi:nil="true"/>
            <environment xsi:type="xsd:string" xsi:nil="true"/>
            <fixVersions soapenc:arrayType="ns1$i0$:RemoteVersion[0]" xsi:type="soapenc:Array"/>
            <id xsi:type="xsd:string">$issue.id$</id>
            <key xsi:type="xsd:string">$issue.key$</key>
            <priority xsi:type="xsd:string">$issue.fields.priority.id$</priority>
            <project xsi:type="xsd:string">$issue.fields.project.key$</project>
            <reporter xsi:type="xsd:string">$issue.fields.reporter.name$</reporter>
            <resolution xsi:type="xsd:string" xsi:nil="true"/>
            <status xsi:type="xsd:string">$issue.fields.status.id$</status>
            <summary xsi:type="xsd:string">$issue.fields.summary; format="xml-encode"$</summary>
            <type xsi:type="xsd:string">$issue.fields.issuetype.id$</type>
            <updated xsi:type="xsd:dateTime">$issue.fields.updated$</updated>
            <votes xsi:type="xsd:long">0</votes>
        </multiRef>
        <multiRef id="version$i0$" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns2$i0$:RemoteVersion" xmlns:ns2$i0$="http://beans.soap.rpc.jira.atlassian.com"
                  xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
            <id xsi:type="xsd:string">$first(issue.fields.versions).id$</id>
        </multiRef>
}; separator="\n"$
    </soapenv:Body>
</soapenv:Envelope>
>>