build() ::= <<
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
        <ns1:getComponentsResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://soap.rpc.jira.atlassian.com">
            <getComponentsReturn soapenc:arrayType="ns2:RemoteComponent[0]" xsi:type="soapenc:Array" xmlns:ns2="http://beans.soap.rpc.jira.atlassian.com"
                                 xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
        </ns1:getComponentsResponse>
    </soapenv:Body>
</soapenv:Envelope>
>>